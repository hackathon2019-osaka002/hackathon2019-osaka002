﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TitleSceneManager : MonoBehaviour
{
    private StageManager sm;
    public SpriteRenderer button;

    private Color myColor;
    private Color finColor;

    public AudioSource audioObj;
    public AudioClip bgmBG;
    public AudioClip buttonSE;
    // Start is called before the first frame update
    void Start()
    {
        sm = GameObject.Find("StageManager").GetComponent<StageManager>();
        myColor = button.color;
        finColor = Color.white;
        finColor.a = 0;
        audioObj.clip = bgmBG;
        audioObj.loop = true;
        audioObj.Play();
    }

    // Update is called once per frame
    void Update()
    {

        button.color = Color.Lerp(myColor, finColor, Mathf.PingPong(Time.time, 1));
        if(Input.anyKeyDown)
        {
            sm.SwitchScene(2);
            audioObj.Stop();
            audioObj.clip = buttonSE;
            audioObj.loop = false;
            audioObj.Play();
        }
    }
}
