﻿using UnityEngine;
[RequireComponent(typeof(CharacterController))]
public class CharacterBase : MonoBehaviour {
    [SerializeField] protected float speed = 0.1f;
    [SerializeField] protected SpriteRenderer playerGraphic = default;
    protected CharacterController characterController;
    protected Vector3 axis;
    protected Quaternion way = Quaternion.Euler(0,0,0) ;

    protected virtual void Start() {
        if (!playerGraphic) {
            playerGraphic = this.GetComponentInChildren<SpriteRenderer>();
        }
        characterController = this.GetComponent<CharacterController>();
    }

    protected virtual void Move() {
        if (axis.x > 0.1f || axis.x < -0.1f || axis.y > 0.1f || axis.y < -0.1f) {
            way = Quaternion.AngleAxis(Mathf.Atan2(axis.x, axis.y) * -Mathf.Rad2Deg, Vector3.forward);
        }
        //this.transform.Translate(axis * speed);
        characterController.Move(axis * speed);
        playerGraphic.transform.rotation = way;
    }
}
