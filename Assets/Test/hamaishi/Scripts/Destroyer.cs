﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Destroyer : MonoBehaviour {
    static Destroyer destroyer;
    [SerializeField] List<GameObject> dontDestroy;
    [SerializeField] Camera mainCamera;
    [SerializeField] float destroyRange = 10f;
    GameObject[] objects;
    private void Start() {
        if (!mainCamera) {
            mainCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
        }
        destroyer = this;
    }
    private void Update() {
        objects = Object.FindObjectsOfType<GameObject>()
            .Except(dontDestroy)
            .ToArray();
        foreach(var item in objects) {
            Vector3 pos = RectTransformUtility.WorldToScreenPoint(mainCamera, item.transform.position);
            if(
                pos.x<-destroyRange||
                pos.x>Screen.width+destroyRange||
                pos.y < -destroyRange ||
                pos.y > Screen.height + destroyRange
                ) {
                if (!dontDestroy.Contains(item.transform.root.gameObject))
                    Destroy(item);
            }
        }
    }
    static public void AddDontDestroy(GameObject gameObject) {
        destroyer.dontDestroy.Add(gameObject);
    }
}
