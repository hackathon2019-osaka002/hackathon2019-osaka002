﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageShadow : MonoBehaviour
{
    public GameObject shadowPrefab;
    private float passedTime;
    private const float maxShadowScale = 5f;
    private const float Interval = 7f;

    void Start()
    {
        passedTime = 3f;
    }

    void Update()
    {
        //一定時間でランダム位置にランダムサイズの影生成
        passedTime += Time.deltaTime;

        if (passedTime > Interval)
        {
            passedTime = 0f;
            var camera_pos = GameObject.FindWithTag("MainCamera").transform.position;
            var new_shadow = Instantiate(shadowPrefab, this.transform);
            new_shadow.transform.position = new Vector3(camera_pos.x + 12f, Random.Range(-3.5f, 3.5f), 0f);
            new_shadow.transform.localScale = new Vector3(Random.Range(1f, maxShadowScale), Random.Range(1f, maxShadowScale), 1f);
        }
    }
}
