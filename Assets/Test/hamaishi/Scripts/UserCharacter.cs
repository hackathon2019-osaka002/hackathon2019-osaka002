﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
[RequireComponent(typeof(Animator))]
public class UserCharacter : CharacterBase {
    [Space(10), SerializeField] GimicHave[] gimics;
    [SerializeField] Text textPrefav;
    int gimicChoice;
    Canvas canvas;
    Camera mainCamera;
    public Text itemtext;
    bool click = false;
    public Animator animator;
    //int animatorID;

    [SerializeField] Text GimicText;

    protected override void Start() {
        base.Start();
        canvas = GameObject.Find("Canvas").GetComponent<Canvas>();
        mainCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
        //animator = this.GetComponent<Animator>();
        //animatorID = Animator.StringToHash("Horizontal");
        if (!GimicText) {
            GimicText = GameObject.Find("GimicText").GetComponent<Text>();
        }
        itemtext.text = "item:" + gimics[gimicChoice].value.ToString();
    }
    bool rMouse = false;
    void Update() {
        itemtext.text = "item:" + gimics[gimicChoice].value.ToString();
        axis = new Vector3(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"), 0);
        Move();
        float wheel = Input.GetAxisRaw("Fire2");
        if (Mathf.Abs(wheel) > 0.01f) {
            if (!rMouse) {
                rMouse = true;
                gimicChoice += wheel > 0 ? 1 : -1;
                //print("gimic:" + gimicChoice);
                if (gimicChoice >= gimics.Length) {
                    //print("0");
                    gimicChoice = 0;
                }
                if (gimicChoice < 0) {
                    //print("m");
                    gimicChoice = gimics.Length - 1;
                }
                var textObj = Instantiate(
                    textPrefav,
                    RectTransformUtility.WorldToScreenPoint(mainCamera, this.transform.position),
                    Quaternion.identity,
                    canvas.transform
                );
                textObj.text = gimics[gimicChoice].Gimic.GetComponent<GimicName>().NAME;
                GimicText.text = gimics[gimicChoice].Gimic.GetComponent<GimicName>().NAME;
                //Destroyer.AddDontDestroy(textObj.gameObject);
                StartCoroutine(TextVanish(textObj));
            }
        } else {
            rMouse = false;
        }
        if (Input.GetAxisRaw("Fire1")>0.1f) {
            if (!click) {
                click = true;
                if (gimics[gimicChoice].value > 0) {
                    gimics[gimicChoice].value--;
                    Instantiate(gimics[gimicChoice].Gimic, this.transform.position, Quaternion.identity);
                    Camera.main.GetComponent<GameSceneManager>().SE01();
                }
            }
        } else {
            click = false;
        }
    }
    protected override void Move() {
        characterController.Move(axis * speed);
        if (axis.x > 0.01f)
        {
            animator.speed = 1;
            animator.SetBool("isDown", false);
            animator.SetBool("isUp", false);
            animator.SetBool("isRight", true);
            animator.SetBool("isLeft", false);
        }
        else if (axis.x < -0.01f)
        {
            animator.speed = 1;
            animator.SetBool("isDown", false);
            animator.SetBool("isUp", false);
            animator.SetBool("isRight", false);
            animator.SetBool("isLeft", true);
        }
        else if(axis.y > 0.01f)
        {
            animator.speed = 1;
            animator.SetBool("isDown", false);
            animator.SetBool("isUp", true);
            animator.SetBool("isRight", false);
            animator.SetBool("isLeft", false);
        }
        else if (axis.y < -0.01f)
        {
            animator.speed = 1;
            animator.SetBool("isDown", true);
            animator.SetBool("isUp", false);
            animator.SetBool("isRight", false);
            animator.SetBool("isLeft", false);
        }
        else
        {
            animator.speed = 0;
        }
    }
    IEnumerator TextVanish(Text text) {
        for(float i = 0; i < 1; i += Time.deltaTime) {
            Color c = text.color;
            c.a = 1 - i;
            text.color = c;
            yield return null;
        }
        Destroy(text.gameObject);
    }

    public GimicHave GetGimic(int index) {
        return gimics[index];
    }
    public void AddGimicValue(int index,int valueAdd) {
        gimics[index].value += valueAdd;
    }
    public void AddGimicValue(int valueAdd) {
        foreach(var item in gimics) {
            item.value++;
            itemtext.text = "item:" + item.value.ToString();
        }
    }
    public int GetGimicIndex(GimicHave gimic) {
        return gimics.Select((item, index) => (item, index)).Where(t => t.item.Gimic == gimic.Gimic).First().index;
    }
}
[System.Serializable]
public class GimicHave {
    public int value;
    [SerializeField]GameObject gimic;
    public GameObject Gimic { get { return gimic; } }
}