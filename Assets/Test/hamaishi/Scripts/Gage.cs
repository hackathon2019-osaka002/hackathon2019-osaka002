﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gage : MonoBehaviour {
    [SerializeField] Image gageContent = default;
    private void Start() {
        if (!gageContent) {
            gageContent = this.GetComponentsInChildren<Image>()[1];
        }
    }
    public void ValueChange(float value) {
        gageContent.transform.localScale = new Vector3(value, 1, 1);
    }
}
