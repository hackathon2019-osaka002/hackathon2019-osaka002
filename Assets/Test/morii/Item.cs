﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public int AddGimic = 1;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "PlayChar")
        {
            Debug.Log("ItemHit!");
            collision.GetComponent<UserCharacter>().AddGimicValue(AddGimic);
            Destroy(gameObject);
        }
    }
}
