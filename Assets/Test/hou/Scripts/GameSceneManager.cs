﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameSceneManager : MonoBehaviour
{
    private StageManager sm;
    private GameSaveData saveData;

    public AudioSource audioObj;
    public AudioClip bgmBG;
    public AudioClip refrectSE;
    public AudioClip winSE;
    public AudioClip loseSE;

    // Start is called before the first frame update
    void Start()
    {
        sm = GameObject.Find("StageManager").GetComponent<StageManager>();
        saveData = GameObject.Find("SaveData").GetComponent<GameSaveData>();
        audioObj.clip = bgmBG;
        audioObj.loop = true;
        audioObj.Play();
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.N))
        //{
        //    saveData.camOffx = Camera.main.transform.position.x;
        //    saveData.camOffy = Camera.main.transform.position.y;
        //    audioObj.Stop();
        //    sm.NextScene();
        //}
    }
    
    public void NextStage(bool win,float hp)
    {
        saveData.health = hp;
        saveData.winFlag = win;
        saveData.camOffx = Camera.main.transform.position.x;
        saveData.camOffy = Camera.main.transform.position.y;
        audioObj.Stop();
        sm.NextScene();
    }

    public void SE01()
    {
        AudioSource.PlayClipAtPoint(refrectSE,Camera.main.gameObject.transform.position);
        //audioObj.clip = refrectSE;
        //audioObj.loop = false;
        //audioObj.Play();
    }

    public void SEWin()
    {
        audioObj.clip = winSE;
        //audioObj.loop = false;
        audioObj.Play();
    }
    public void SELose()
    {
        audioObj.clip = loseSE;
        //audioObj.loop = false;
        audioObj.Play();
    }
}
