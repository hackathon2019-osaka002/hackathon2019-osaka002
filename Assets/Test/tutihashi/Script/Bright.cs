﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bright : MonoBehaviour
{
    private GameObject mainChara;
    void Start()
    {
        mainChara = GameObject.FindWithTag("MainChar");
    }

    void Update()
    {
        var dist = transform.position.x - 6.5f   - mainChara.transform.position.x;
        if(dist < 5f){
            var alpha = (225f - (5f-dist) * 45f) / 255f;
            GetComponent<SpriteRenderer>().color = new Color(0f, 0f, 0f, alpha);
            //Debug.Log(alpha);
        }
    }
}
