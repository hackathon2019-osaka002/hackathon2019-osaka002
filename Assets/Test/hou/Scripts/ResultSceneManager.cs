﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultSceneManager : MonoBehaviour
{
    private StageManager sm;
    private GameSaveData saveData;

    public Text resultText;
    public SpriteRenderer button;
    public Text scores;

    private Color myColor;
    private Color finColor;

    public AudioSource audioObj;
    public AudioClip bgmBG;
    public AudioClip buttonSE;

    // Start is called before the first frame update
    void Start()
    {
        sm = GameObject.Find("StageManager").GetComponent<StageManager>();
        saveData = GameObject.Find("SaveData").GetComponent<GameSaveData>();
        if(saveData.winFlag)
        {
            resultText.text = "Win";
            resultText.color = Color.yellow;
            float hp = saveData.health * 10.0f;

            if (hp > 70)
            {
                scores.text = "強い希望を届けた！";
            }
            else if (hp > 40)
            {
                scores.text = "希望を届けた.";
            }
            else
            {
                scores.text = "淡い希望を届けた";
            }
            //scores.text = "HP : " + hp.ToString("f2") + "%";
        }
        else
        {
            resultText.text = "希望を届けられなかった…";
            resultText.color = Color.red;
            scores.text = "Scores : " + saveData.camOffx.ToString();
        }
        myColor = button.color;
        finColor = Color.white;
        finColor.a = 0;
        audioObj.clip = bgmBG;
        audioObj.loop = true;
        audioObj.Play();
    }

    // Update is called once per frame
    void Update()
    {
        button.color = Color.Lerp(myColor, finColor, Mathf.PingPong(Time.time, 1));
        if (Input.anyKeyDown)
        {
            sm.BackToTitle();
            audioObj.Stop();
            audioObj.clip = buttonSE;
            audioObj.loop = false;
            audioObj.Play();
        }
    }
}
