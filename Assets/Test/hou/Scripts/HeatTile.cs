﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeatTile : MonoBehaviour
{
    public bool destroyFlag = false;
    public float time = 5.0f;
    public float delTime = 0.0f;
    // Start is called before the first frame update
    void Start()
    {
        delTime = time;
    }

    // Update is called once per frame
    void Update()
    {
        //delTime = time - Time.deltaTime;
        if (destroyFlag)
        {
            delTime -= Time.deltaTime;
            if (delTime < 0)
            {
                GameObject.Destroy(this.gameObject);
            }
        }
        else
        {
            delTime += Time.deltaTime;
            if (delTime > time)
            {
                delTime = time;
            }
        }
    }
   
}