﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    void Start()
    {
        
    }

    void Update()
    {
        
    }
    void OnTriggerEnter( Collider other)
    {
        if(other.tag == "Shadow"){
            GetComponent<SpriteRenderer>().color = new Color(0.5f,0.5f,0.5f);
        }
    }

    void OnTriggerExit( Collider other)
    {
        if(other.tag == "Shadow"){
            GetComponent<SpriteRenderer>().color = new Color(1f,1f,1f);
        }
    }
}
