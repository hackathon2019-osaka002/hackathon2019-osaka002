﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserAnimation : MonoBehaviour
{
    public Animator user;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            user.speed = 0;
        }
            if (Input.GetKeyDown(KeyCode.V))
        {
            user.speed = 1;
            user.SetBool("isDown", true);
            user.SetBool("isUp", false);
            user.SetBool("isRight", false);
            user.SetBool("isLeft", false);
        }
        if (Input.GetKeyDown(KeyCode.B))
        {
            user.speed = 1;
            user.SetBool("isDown", false);
            user.SetBool("isUp", true);
            user.SetBool("isRight", false);
            user.SetBool("isLeft", false);
        }
        if (Input.GetKeyDown(KeyCode.N))
        {
            user.speed = 1;
            user.SetBool("isDown", false);
            user.SetBool("isUp", false);
            user.SetBool("isRight", true);
            user.SetBool("isLeft", false);
        }
        if (Input.GetKeyDown(KeyCode.M))
        {
            user.speed = 1;
            user.SetBool("isDown", false);
            user.SetBool("isUp", false);
            user.SetBool("isRight", false);
            user.SetBool("isLeft", true);
        }
    }
}
