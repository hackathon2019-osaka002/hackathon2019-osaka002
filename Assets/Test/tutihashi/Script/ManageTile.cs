﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageTile : MonoBehaviour
{
    public GameObject tilePrefab;
    private float nextTileX;

    //private Vector2 
    void Start()
    {
        var cam_pos = GameObject.FindWithTag("MainCamera").transform.position;
        var tile_size = tilePrefab.GetComponent<Renderer>().bounds.size;
        
        //プレイヤーを中心にタイルを用意
        for (var i = 0; i < 30; i++)
        {
            for (var j = 0; j < 16; j++)
            {
                var new_tile = Instantiate(tilePrefab, transform);
                new_tile.transform.localPosition = new Vector3(cam_pos.x - 8.6f + i * tile_size.x, cam_pos.y + 4.7f - j * tile_size.y, 0f);
            }
        }
        nextTileX = cam_pos.x - 8.6f + 30 * tile_size.x;
        
    }

    void Update()
    {
        //右端に追加
        if (this.transform.childCount < 30*16)
        {
            var cam_pos = GameObject.FindWithTag("MainCamera").transform.position;
            var tile_size = tilePrefab.GetComponent<Renderer>().bounds.size;
            for (var i = 0; i < 16; i++)
            {
                var new_tile = Instantiate(tilePrefab, transform);
                new_tile.transform.localPosition = new Vector3(nextTileX, cam_pos.y + 4.7f - i * tile_size.y, 0f);
            }
            nextTileX += tile_size.x;
        }
    }
}
