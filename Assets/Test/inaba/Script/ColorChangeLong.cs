﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChangeLong : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "tile")
        {
            float H, S, V;

            GameObject tile = collision.gameObject;
            Renderer renderer = tile.GetComponent<Renderer>();
            Color color = renderer.material.color;
            Color.RGBToHSV(new Color(color.r, color.g, color.b, 1.0F), out H, out S, out V);
            //renderer.material.color = Color.HSVToRGB(0.5f, 1.0f, 1.0f);

            int ReH = (int)(H * 360) + 10;

            if (ReH > 360)
            {
                ReH = 360;
            }

            Color color2 = ColorHSV.FromHsv(ReH, 255, 255);

            renderer.material.color = color2;
            Debug.Log(H);
            Debug.Log(ColorHSV.FromHsv(100, 100, 255));
            Debug.Log(ColorHSV.FromHsv(200, 100, 255));
        }

        if (collision.gameObject.tag == "MainChar")
        {
            float H, S, V;

            GameObject tile = collision.gameObject;
            Renderer renderer = tile.GetComponent<Renderer>();
            Color color = renderer.material.color;
            Color.RGBToHSV(new Color(color.r, color.g, color.b, 1.0F), out H, out S, out V);
            //renderer.material.color = Color.HSVToRGB(0.5f, 1.0f, 1.0f);

            int ReH = (int)(H * 360) + 10;

            if (ReH > 360)
            {
                ReH = 360;
            }

            Color color2 = ColorHSV.FromHsv(ReH, 255, 255);

            //renderer.material.color = color2;
            renderer.material.color = renderer.material.color + color2;
            Debug.Log(H);
            Debug.Log(ColorHSV.FromHsv(100, 100, 255));
            Debug.Log(ColorHSV.FromHsv(200, 100, 255));
        }
    }

}
