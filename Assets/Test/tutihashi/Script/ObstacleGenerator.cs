﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleGenerator : MonoBehaviour
{
    public GameObject objectPrefab;
    public GameObject shadowPrefab;
    public GameObject lightPrefab;
    public GameObject itemPrefab;
    private float passedTime;

    [SerializeField]
    private float minGenInterval = 5f;
    [SerializeField]
    private float maxGenInterval = 10f;
    [SerializeField]
    private float maxShadowScale = 2f;

    [SerializeField]
    private int objectRatio;
    [SerializeField]
    private int shadowRatio;
    [SerializeField]
    private int lightRatio;
    [SerializeField]
    private int itemRatio;

    void Start()
    {
        passedTime = 6f;
    }

    void Update()
    {
        //どちらかを生成
        passedTime += Time.deltaTime;

        if (Random.Range(0f,maxGenInterval-minGenInterval) < passedTime - minGenInterval){

            var rand = Random.Range(1, objectRatio + shadowRatio + lightRatio + itemRatio + 1);
            if(rand <= objectRatio){
                GenerateObject();
            }
            else if(rand <= objectRatio + shadowRatio){
                GenerateShadow();          
            }
            else if(rand <= objectRatio + shadowRatio + lightRatio){
                GenerateLight();
            }
            else if(rand <= objectRatio + shadowRatio + lightRatio + itemRatio){
                GenerateItem();
            }
            passedTime = 0f;
        }
    }

    //アイテム生成
    private void GenerateItem()
    {
        var camera_pos = GameObject.FindWithTag("MainCamera").transform.position;
        var new_object = Instantiate(itemPrefab, this.transform);
        new_object.transform.position = new Vector3(camera_pos.x + 12f, Random.Range(-4.5f, 4.5f), 0f);    
    }

    //ライト生成
    private void GenerateLight()
    {
        var camera_pos = GameObject.FindWithTag("MainCamera").transform.position;
        var new_object = Instantiate(lightPrefab, this.transform);
        new_object.transform.position = new Vector3(camera_pos.x + 12f, Random.Range(-4.5f, 4.5f), 0f);    
    }

    // オブジェクト生成
    private void GenerateObject()
    {
        var camera_pos = GameObject.FindWithTag("MainCamera").transform.position;
        var new_object = Instantiate(objectPrefab, this.transform);
        new_object.transform.position = new Vector3(camera_pos.x + 12f, Random.Range(-4.5f, 4.5f), 0f);    
    }

    // ランダム位置にランダムサイズの影生成
    private void GenerateShadow()
    {   
        var camera_pos = GameObject.FindWithTag("MainCamera").transform.position;
        var new_shadow = Instantiate(shadowPrefab, this.transform);
        new_shadow.transform.position = new Vector3(camera_pos.x + 12f, Random.Range(-3.5f, 3.5f), 0f);
        new_shadow.transform.localScale = new Vector3(Random.Range(1f, maxShadowScale/2f), Random.Range(1f, maxShadowScale), 1f);
    }
}
