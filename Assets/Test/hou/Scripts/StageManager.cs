﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StageManager : MonoBehaviour
{
    public int currentlyAtScene;

    public Texture2D fadeOutTexture;
    public float fadeSpeed = 0.8f;

    private int drawDepth = -1000;
    private float alpha = 1.0f;
    private int fadeDir = -1;

    private bool switchingFlg;

    public int maxScene;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        SceneManager.sceneLoaded += OnLevelFinishedLoading;

        currentlyAtScene = 0;
        switchingFlg = false;
        //Screen.SetResolution(1920, 1080, true);
    }

    // Use this for initialization
    void Start()
    {
        currentlyAtScene = 1;
        SceneManager.LoadScene(1, LoadSceneMode.Single);
    }

    //　サンプル
    //  他のクラスを使う　例
    //　private StageManager sm;
    //  sm = GameObject.Find("StageManager").GetComponent<StageManager>();
    //  sm.NextScene();
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
        if (Input.GetKeyDown(KeyCode.F1))
        {
            ReloadScene();
        }
        if (Input.GetKeyDown(KeyCode.F11))
        {
            NextScene();
        }
        if (Input.GetKeyDown(KeyCode.F2))
        {
            BackToTitle();
        }
    }

    /// <summary>
    /// 再読み込み
    /// </summary>
    public void ReloadScene()
    {
        SwitchScene(currentlyAtScene);
    }

    /// <summary>
    /// シーンを読み込み
    /// </summary>
    /// <param name="s">シーンの名前</param>
    public void SwitchScene(string s)
    {
        if (!switchingFlg)
        {
            switchingFlg = true;
            StartCoroutine(DoLoadScene(s));
        }
    }

    /// <summary>
    /// シーンを読み込み
    /// </summary>
    /// <param name="s">シーンの番号</param>
    public void SwitchScene(int s)
    {
        if (!switchingFlg)
        {
            switchingFlg = true;
            currentlyAtScene = s;
            StartCoroutine(DoLoadScene(s));
        }
    }

    /// <summary>
    /// タイトルを戻る
    /// </summary>
    public void BackToTitle()
    {
        if (!switchingFlg)
        {
            switchingFlg = true;
            currentlyAtScene = 1;
            StartCoroutine(DoLoadScene("Title"));
        }
    }

    /// <summary>
    /// 次のシーン
    /// </summary>
    public void NextScene()
    {
        if (!switchingFlg)
        {
            switchingFlg = true;
            if (currentlyAtScene >= maxScene - 1)
                currentlyAtScene = 0;
            StartCoroutine(DoLoadScene(++currentlyAtScene));
        }
    }

    /// <summary>
    /// 前のシーン
    /// </summary>
    public void PreviousScene()
    {
        if (!switchingFlg)
        {
            switchingFlg = true;
            if (currentlyAtScene < 2)
                currentlyAtScene = 2;
            StartCoroutine(DoLoadScene(--currentlyAtScene));
        }
    }

    private IEnumerator DoLoadScene(string s)
    {
        BeginFade(1);
        yield return new WaitForSeconds(fadeSpeed);
        SceneManager.LoadScene(s, LoadSceneMode.Single);
    }
    private IEnumerator DoLoadScene(int s)
    {
        BeginFade(1);
        yield return new WaitForSeconds(fadeSpeed);
        SceneManager.LoadScene(s, LoadSceneMode.Single);
    }

    void OnGUI()
    {
        alpha += fadeDir * fadeSpeed * Time.deltaTime;
        alpha = Mathf.Clamp01(alpha);

        GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, alpha);
        GUI.depth = drawDepth;
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), fadeOutTexture);
    }

    public float BeginFade(int direction)
    {
        fadeDir = direction;
        return fadeSpeed;
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        BeginFade(-1);
        switchingFlg = false;
    }
}
