﻿using System;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;

[RequireComponent(typeof(Animator))]
public class MainCharacter : CharacterBase {
    [Space(10), SerializeField, Tooltip("単位は秒")] float healthMax = 10;
    [SerializeField] Gage healthGage = default;
    Animator animator;
    int animatorID;
    Vector3 lastPos;
    [SerializeField] float errorDist = 0.01f;
    [SerializeField] float stopTime = 1f;
    float timeWait = 0;
    public float health { get; private set; }
    private void Awake() {
        health = healthMax;
    }
    protected override void Start() {
        base.Start();
        if (!healthGage) {
            try {
                healthGage = GameObject.Find("Gage").GetComponent<Gage>();
            } catch (Exception) {
                print("ヘルスゲージが存在しません");
            }
        }
        animator = this.GetComponent<Animator>();
        animatorID = Animator.StringToHash("health");
    }
    void Update() {
        if (health <= 0)
            return;
        axis = new Vector3(1, 0, 0);
        Move();

        if (Vector3.Distance(lastPos, this.transform.position) < errorDist) {
            timeWait += Time.deltaTime;
            if (timeWait > stopTime) {
                //ここにゲームオーバー処理
                Camera.main.GetComponent<GameSceneManager>().NextStage(false, health);
            }
        } else {
            timeWait = 0;
        }
        lastPos = this.transform.position;

        animator.SetFloat(animatorID, health / healthMax);
    }
    private void OnTriggerStay(Collider other) {
        if (health <= 0)
            return;

        var triggers = Physics.OverlapBox(transform.position, Vector3.zero);
        if (triggers.Any(t => t.tag == "Goal")) {
            print("GOAL!!");
            //ここにゲームクリア時の処理
            Camera.main.GetComponent<GameSceneManager>().NextStage(true, health);
            Camera.main.GetComponent<GameSceneManager>().SEWin();
        }
        if (triggers.Any(t => t.tag == "Refrect")) {
            return;
        }
        if (triggers.Any(t => t.tag == "Shadow")) {
            this.health -= Time.deltaTime;
            //print("health:" + this.health);
            try {
                healthGage.ValueChange(health / healthMax);
            } catch (Exception) {
                print("ヘルスゲージが見つかりません");
            }
            if (health <= 0) {
                print("GAME OVER");
                //ここにゲームオーバー処理
                Camera.main.GetComponent<GameSceneManager>().NextStage(false, health);
                Camera.main.GetComponent<GameSceneManager>().SELose();
            }
        }
    }
}