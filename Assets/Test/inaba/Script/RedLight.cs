﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedLight : MonoBehaviour
{
    public float col = 0.2f;
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "tile")
        {
            
        }
        if (collision.gameObject.tag == "MainChar")
        {
            float H, S, V;

            GameObject mainchar = collision.gameObject;
            Renderer renderer = mainchar.GetComponent<Renderer>();
            Color color = renderer.material.color;
            Color.RGBToHSV(new Color(color.r, color.g, color.b, 1.0F), out H, out S, out V);
            //renderer.material.color = Color.HSVToRGB(0.5f, 1.0f, 1.0f);

            int ReH = (int)(H * 360) - 10;

            if (ReH < 0)
            {
                ReH = 0;
            }

            Color color2 = ColorHSV.FromHsv(ReH, 255, 255);
            Color color3 = new Color(col, 0, 0);
            renderer.material.color = renderer.material.color + color3;
            Debug.Log(H);
            Debug.Log(ColorHSV.FromHsv(100, 100, 255));
            Debug.Log(ColorHSV.FromHsv(200, 100, 255));
        }
    }
}
