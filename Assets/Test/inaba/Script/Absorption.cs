﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Absorption : MonoBehaviour
{
    public GameObject road;
    // Start is called before the first frame update
    void Start()
    {
        
        
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag=="tile")
        {
            float H, S, V;

            GameObject tile=collision.gameObject;
            Renderer renderer = tile.GetComponent<Renderer>();
            Color color = renderer.material.color;
            Color.RGBToHSV(new Color(color.r, color.g, color.b, 1.0F), out H, out S, out V);
            renderer.material.color = Color.HSVToRGB(H, S, 0.5f);
            // renderer.material.color = Color.HSVToRGB(1.0f, 1.0f, 0.5f);
            // color.r = 1.0f;  // RGBのR(赤)値
            //color.g = 1.0f;  
            //renderer.material.color = color; // 変更した色情報に変更

            collision.gameObject.tag="Shadow";
            Debug.Log("Enter");
        }
    }

    private void OnTriggerExit(Collider collision)
    {
        if (collision.gameObject.tag == "Shadow")
        {
            collision.gameObject.tag = "tile";
            Debug.Log("Exit");
        }
    }
}
