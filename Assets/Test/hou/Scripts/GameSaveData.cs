﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSaveData : MonoBehaviour
{
    // true = クリア
    // flase = GameOver
    public bool winFlag;
    public float camOffx;
    public float camOffy;
    public float health;
    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
