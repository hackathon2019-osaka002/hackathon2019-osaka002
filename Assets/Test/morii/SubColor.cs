﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SubColor : MonoBehaviour
{
    public float SubSpeedMax = 1;

    private float[] SubSpeed = new float[3];

    float stayTime = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEneter(Collider collision)
    {
        var triggers = Physics.OverlapBox(transform.position, Vector3.zero);

        Debug.Log("Hit:SubColor:Init");

        // 初回に減衰数を決める
        if (triggers.Any(t => t.tag == "Shadow"))
        {
            for(int i = 0; i < SubSpeed.Length; i++)
            {
                SubSpeed[i] = Random.Range(0, SubSpeedMax) * Time.deltaTime;
            }
        }
    }
    private void OnTriggerStay(Collider collision)
    {

        var triggers = Physics.OverlapBox(transform.position, Vector3.zero);

        Debug.Log("Hit:SubColor:Stay");

        // 減衰
        if (triggers.Any(t=>t.tag=="Shadow"))
        {
            if (stayTime < 0.0000001f) {
                for (int i = 0; i < SubSpeed.Length; i++) {
                    SubSpeed[i] = Random.Range(0, SubSpeedMax);
                }
            }
            if (stayTime < 0.1f) {
                transform.Find("Graphic").GetComponent<SpriteRenderer>().color -= new Color(SubSpeed[0] / 10, SubSpeed[1] / 10, SubSpeed[2] / 10, 0);
            }
            stayTime += Time.deltaTime;
        } else {
            stayTime = 0;
        }
    }
}
