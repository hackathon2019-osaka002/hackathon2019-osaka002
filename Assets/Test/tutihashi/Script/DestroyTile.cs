﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyTile : MonoBehaviour
{
    void Update()
    {
        //画面外ならば破棄
        var go = GameObject.FindWithTag("MainCamera");
        
        if (transform.position.x < go.transform.position.x -9.3f )
        {
            GameObject.Destroy(this.gameObject);
        }
    }
}
