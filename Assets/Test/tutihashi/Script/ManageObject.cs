﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageObject : MonoBehaviour
{
    public GameObject objectPrefab;
    private float passedTime;
    private const float Interval = 6f;

    void Start()
    {
        passedTime = 3f;
    }

    void Update()
    {
        //一定時間でランダム位置にランダムサイズの影生成
        passedTime += Time.deltaTime;

        if (passedTime > Interval)
        {
            passedTime = 0f;
            var camera_pos = GameObject.FindWithTag("MainCamera").transform.position;
            var new_object = Instantiate(objectPrefab, this.transform);
            new_object.transform.position = new Vector3(camera_pos.x + 12f, Random.Range(-4.5f, 4.5f), 0f);    
        }
    }
}
