﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Abs_Light : MonoBehaviour
{
    public bool destroyFlag = false;
    public float downtime = 5.0f;
    public float uptime = 2.0f;
    public float delTime = 0.0f;
    public float delTime2 = 0.0f;
    SpriteRenderer renderer;
    private float sizeX;

    private float sizeY;

    private float flag=0;

    public float hani_down = 1;
    public float hani_up = 2;
    // Start is called before the first frame update
    void Start()
    {
        delTime = downtime;
        delTime2 = 0;


        renderer = this.GetComponent<SpriteRenderer>();
        //sizeX = renderer.bounds.size.x;
        //sizeY = renderer.bounds.size.y;
        sizeX = 4;
        sizeY = 4;
        Debug.Log(sizeX);
        Debug.Log(sizeY);

    }

    // Update is called once per frame
    void Update()
    {
        
        
        if (delTime2 < uptime&&flag==1)
        {
            delTime2 += Time.deltaTime;
            this.transform.localScale = new Vector3(sizeX * (delTime2 / uptime)*hani_up, sizeY * (delTime2 / uptime)*hani_up, 1.0f);
            renderer.color = new Color(1f,1f,1f,0.5f);
            this.gameObject.tag = "Refrect";
            Debug.Log("up");
        }
        if (delTime > 0)
        {
            delTime -= Time.deltaTime;
            if (delTime < 0) { delTime = 0; flag = 1; }
            this.transform.localScale = new Vector3(sizeX * (delTime / downtime)/hani_down, sizeY * (delTime / downtime)/hani_down, 1.0f);
            Debug.Log("dow");
        }

    }

    /*
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "tile" && delTime > 0)
        {

            collision.gameObject.tag = "Shadow";
            Debug.Log("Enter");
        }
        if (collision.gameObject.tag == "Shadow" && flag == 1)
        {
            collision.gameObject.tag = "tile";
        }
    }
    */
    private void OnTriggerStay(Collider collision)
    {
        if (collision.gameObject.tag == "tile" && delTime > 0)
        {

            collision.gameObject.tag = "Shadow";
            Debug.Log("Enter");
        }
        if (collision.gameObject.tag == "Shadow" && flag == 1)
        {
            collision.gameObject.tag = "tile";
        }
    }
    private void OnTriggerExit(Collider collision)
    {
        if (collision.gameObject.tag == "Shadow")
        {
            collision.gameObject.tag = "tile";
            Debug.Log("Exit");
        }
    }
}
